# GMR-git

## What is this?

Civ V GMR savefile watcher. Will keep a history of GMR savefiles. Initially will just copy them over to another location and name them by their md5, but ultimately will invoke git to keep them in a repo.

## How do I use it?

* Clone repository
* Remove the `.example` extension from all files in `config/`, and make sure to correct the values in them according to your system.
* Run `./main.sh`

## Anything to consider?

You might want to run it in `tmux` or `disown ./main.sh &>/dev/null &` if you don't care about the output, and want it to run in the "background"

## LICENSE

MIT

## Authors

Axel Latvala, 2019
