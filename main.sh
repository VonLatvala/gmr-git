#!/bin/bash
SCRIPT_PATH="$( cd "$(dirname "$0")" ; pwd -P )"
REL_CONFIG_DIR_PATH="config"
GMR_HOTSEAT_PATH="/path/to/non/set/hotseat/path"
GMR_SAVEFILE_NAME="inexistent.invalid"
GMR_SAVEFILE_FULL_PATH="not_set.invalid"

bootup() {
    GMR_HOTSEAT_PATH="$(cat "$SCRIPT_PATH/$REL_CONFIG_DIR_PATH/hotseatdir.txt")"
    GMR_SAVEFILE_NAME="$(cat "$SCRIPT_PATH/$REL_CONFIG_DIR_PATH/gmrsavefile.txt")" 
    GMR_SAVEFILE_FULL_PATH="$GMR_HOTSEAT_PATH/$GMR_SAVEFILE_NAME"
}
timestamp() {
    echo -n $(date +"[%Y.%m.%d %H:%M:%S]") 
}

bootup

echo "$(timestamp) USING SAVEFILE '$GMR_HOTSEAT_PATH/$GMR_SAVEFILE_NAME'"

lastmd5="NOT_SET"
while [[ 0 ]]; do
    if [[ "$lastmd5" == "NOT_SET" ]]; then
        echo "$(timestamp) First pass, recording initial md5"
        lastmd5="$(md5sum "$GMR_SAVEFILE_FULL_PATH" | awk '{print $1}')"
        echo "$(timestamp) Watchdog started!"
        continue
    fi
    curmd5="$(md5sum "$GMR_SAVEFILE_FULL_PATH" | awk '{print $1}')"
    if [[ "$lastmd5" == "$curmd5" ]]; then
        # NOOP
        :
    else
        bufile="$GMR_HOTSEAT_PATH/gmr-git-$(date +%y%m%d-%H%M%S)-$curmd5.Civ5Save"
        echo 
        echo -n "$(timestamp) file changed. New MD5 '$curmd5', saving to '$bufile'..."
        cp "$GMR_SAVEFILE_FULL_PATH" "$bufile"
        lastmd5="$curmd5"
        if [[ $? -eq 0 ]]; then
            echo "OK!"
        else
            echo "Error"
        fi
    fi
    sleep 1
done
